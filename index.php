<!DOCTYPE html>
<html class="no-js" lang="fr">
  <?php include "head.php" ?>
  <body>

    <script>
    document.querySelector('html').classList.remove('no-js');

    function toggleTheme() {
        document.querySelector('body').classList.toggle('themeLight');
    }

    function toggleModal() {
      document.querySelector('.modalForm').classList.toggle('modalForm--open');
    }
    </script>

    <header class="header">
      <h1 class="header__title">Courage aux soignant·e·s !</h1>

      <?php

      	  include 'db.inc.php';
          $femmes = array('👩','👩🏻','👩🏼','👩🏽','👩🏾','👩🏿');
          $hommes = array('👨🏿','👨🏾','👨🏽','👨🏼','👨🏻','👨');

          $compteur = pg_query_params($dbconn, 
          	  "SELECT count(id) as total
            	   FROM commentaire 
           	   WHERE visible = true", array());
	  $row = pg_fetch_row($compteur);

          echo '<p class="header__emoji">'.$femmes[rand(0,count($femmes)-1)].'‍⚕️ 💖 '.$hommes[rand(0,count($hommes)-1)].'‍⚕️</p>';

      
      	  echo '<p class="header__count">Déjà '.$row[0].' messages !</p>';
      ?>

      <button class="btnTheme" onclick="toggleTheme()">
        <span class="fa fa-sun-o">&nbsp;</span>
        Changer de thème
        <span class="fa fa-moon-o">&nbsp;</span>
      </button>
    </header>
    
    <button class="btnAdd" onclick="toggleModal()"><span class="fa fa-plus">&nbsp;</span> Ajouter un message</button>

    <main class="main">
      <?php

      /* On récupère les commentaires visibles */
      $result = pg_query_params($dbconn, 
            "SELECT texte,nom,to_char(date,'dd/mm HH24:MI')
            FROM commentaire 
            WHERE visible = true 
            ORDER BY id DESC", array());

      while ( $row = pg_fetch_row($result) )
      {

        echo '
          <article class="card">
            <p class="card__name">'.$row[1].'</p>
            <p class="card__date">'.$row[2].'</p>
            <p class="card__txt">« '.$row[0].' »</p>
          </article>';

      }
      ?>
    </main>

    <section class="modalForm">
      <form class="modalForm__form" action="post.php" method="post">

        <label class="modalForm__label" for="nom">Votre nom <small>(vide = anonyme)</small></label>
        <input class="modalForm__input" id="nom" type="text" name="nom">

        <label class="modalForm__label" for="texte">Votre petit mot</label>
        <textarea class="modalForm__input" id="texte" name="texte" cols="25" rows="3"></textarea>

        <footer class="modalForm__footer">
          <button type="button" class="modalForm__btn modalForm__btn--close" onclick="toggleModal()">Annuler l’action</button>
          <input class="modalForm__btn modalForm__btn--submit" type="submit" value="Envoyer le message">
        </footer>

      </form>
    </section>

    <p class="info">
      <a href="https://framagit.org/soleneBSD/commentaires-soignants">Code source</a>
      |
      <a href="/legal.php">Mentions légales</a>
    </p>

  </body>
</html>
