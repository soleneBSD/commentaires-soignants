<head>
    <title>Merci aux soignant·e·s</title>
    <meta charset="UTF-8">
    <meta name="description" content="Encouragez et remerciez les soignant·e·s par un petit mot. Iels sont en première ligne contre le COVID-19. Apportez leur un peu de soutien et de réconfort. Montrez leur que vous pensez à elles et eux.">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="theme-color" content="#fdcb58">
    <meta name="author" content="Tom23 [@Tom23@mastodon.xyz]">
    <meta name="author" content="Solene [@solene@bsd.network ]">
    <meta name="author" content="AntoineÐ [@antoined@kher.nl]">
    <link rel="stylesheet" type="text/css" href="style.css" />
    <link rel="apple-touch-icon" sizes="180x180" href="favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="favicon/favicon-16x16.png">
    <link rel="manifest" href="favicon/site.webmanifest">
  </head>
