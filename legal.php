<!DOCTYPE html>
<html lang="fr">
  <?php include "head.php" ?>
<body>

  <header class="header">
    <h1 class="header__title">Mentions légales</h1>

  <main class="main">


    <article class="card">
      <p class="card__txt">Le site est hébergé sur un serveur loué chez <a hre="https://oneprovider.com">BrainStorm Network - OneProvider</a> situé à l'adresse « 3275 Av Francis-Hughes, Laval, QC H7L 5A5 Canada », joignable par téléphone au +1 (514) 2860253</p>
      <p class="card__txt">Les sources du site sont disponibles à l'adresse <a href="https://framagit.org/soleneBSD/commentaires-soignants">https://framagit.org/soleneBSD/commentaires-soignants</a> sous licence libre BSD 2-clauses.</p>
    </article>

  </main>

  <section class="modalForm">
    <form class="modalForm__form" action="post.php" method="post">

      <label class="modalForm__label" for="nom">Votre nom <small>(vide = anonyme)</small></label>
      <input class="modalForm__input" id="nom" type="text" name="nom">

      <label class="modalForm__label" for="texte">Votre petit mot</label>
      <textarea class="modalForm__input" id="texte" name="texte" cols="25" rows="3"></textarea>

      <footer class="modalForm__footer">
        <button type="button" class="modalForm__btn modalForm__btn--close" onclick="toggleModal()">Annuler l’action</button>
        <input class="modalForm__btn modalForm__btn--submit" type="submit" value="Envoyer le message">
      </footer>

    </form>
  </section>

  <p class="info">
  <a href="https://framagit.org/soleneBSD/commentaires-soignants">Code source</a>
  |
  <a href="/legal">Mentions légales</a>
  </p>

</body>
</html>
