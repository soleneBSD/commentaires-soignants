<!DOCTYPE html>
<html lang="fr">
  <head>
    <title>Moderation</title>
    <meta charset="UTF-8">
	
    <style>
th {
    background-color: lightgrey;
    padding-left: 4px;
    padding-right: 6px;
    border: solid 1px black;
}
table {
    border-collapse: collapse;
    border: solid 1px black;
}
tr:hover {
    background-color: #FAF;
}
    </style>
  </head>
  <body>

  <table id="listes">
    <tr>
	<th>Actions</th>
	<th>Date</th>
	<th>Nom</th>
	<th>Message</th
    ></tr>

<?php

include 'db.inc.php';

if ($_GET['key'] !== "$key")
	exit(0);

if (! empty($_GET['id']))
{
	if (! empty($_GET['action']))
	{
		if($_GET['action'] == "valider")
			$result = pg_query_params($dbconn, 'UPDATE commentaire SET visible = true WHERE id = $1', array($_GET['id']));
		if($_GET['action'] == "supprimer")
			$result = pg_query_params($dbconn, 'DELETE FROM commentaire WHERE id = $1', array($_GET['id']));
		$res = pg_fetch_array($result);
	}
}


/* On récupère les commentaires visibles */
$result = pg_query_params($dbconn, "SELECT id,texte,nom,to_char(date,'dd/mm HH24:MI') FROM commentaire WHERE visible = false", array());
while($row = pg_fetch_row($result)) {
	echo "
	<tr>
		<td>
			<a href=\"moderation.php?action=valider&key=".$_GET['key']."&id=".$row[0]."\">Valider</a>
			<a onclick=\"if (! confirm('Continue?')) { return false; }\" href=\"moderation.php?action=supprimer&key=".$_GET['key']."&id=".$row[0]."\">Supprimer</a>
		</td>
		<td>$row[3]</td>
		<td>$row[2]</td>
		<td>$row[1]</td>
	</tr>";
}


?>
    <script src="datatable.js" type="text/javascript" ></script>
    <script type="text/javascript">var datatables = datatable_autoload();</script>
</body>
</html>
