<?php

include 'db.inc.php';

if (!empty($_POST['texte']))
{

	// si pas de nom alors anonyme
	if (empty($_POST['nom']))
		$nom = "anonyme";
	else
		$nom = filter_var(substr($_POST['nom'],0,200) , FILTER_SANITIZE_STRING);

	$texte = filter_var($_POST['texte'], FILTER_SANITIZE_FULL_SPECIAL_CHARS);

	$result = pg_query_params($dbconn, 'INSERT INTO commentaire (texte,nom) values ($1,$2) returning id', array($texte,$nom));
	$res = pg_fetch_array($result);

echo '<html>
	<head>
		<meta http-equiv="refresh" content="3;url=http://merciauxsoignants.me" />
	</head>
	<body>
		<p>Votre message a bien été reçu, il apparaîtra après validation par un modérateur.</p>
		<p>Merci beaucoup pour votre soutien !</p>
		<p>Redirection en cours vers le site, veuillez patienter quelques secondes</p>
	</body>
</html>';


} else { echo "Erreur, un parametre est manquant"; print_r($_POST); die(); }


?>
